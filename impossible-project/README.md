# Projet Back-end Creature

## Consignes

Vous devrez développer un certain nombre d'endpoints qui serviront lors du développement d'un jeu mobile appelé "Impossible Creatures". Vous en trouverez le cahier des charges plus bas.

Pour faciliter son déroulement, le projet est découpé en étapes basées sur le type de ressource manipulées. Vous n'êtes pas dans l'obligation de suivre le sujet dans l'ordre, à l'exception des bonus qui devront être abordés à la fin du projet.

Vous devrez obligatoirement push le schéma de votre base de données sous format image ou PDF.

## Utilisation (Installé au préalable)

1. IDE: Visual Studio Code 

2. Postman Back-end PHP request en JSON.

3. php 7.2, 7.3, 7.4

4. Framework Laravel



## PDF Base de Données.

<IMG SRC="Schema_impossible_creatures.PNG" width200/>

