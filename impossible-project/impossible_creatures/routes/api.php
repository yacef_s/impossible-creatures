<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AnimalController;
use App\Http\Controllers\SpeciesController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

#login
Route::get('/users', [UserController::class, 'getAll']);

Route::post('/register',[AuthController::class, 'register']);

Route::post('/login', [AuthController::class, 'login']);

Route::post('/logout', [AuthController::class, 'logout']);


#Species

Route::post('/species', [SpeciesController::class, 'create']);

#Animals
Route::get('/animals', [AnimalController::class, 'getAll']);

Route::get('/users/{id}/animals', [AnimalController::class, 'getAll']);

Route::get('/animals/{id}', [AnimalController::class, 'getByID']);


#CREATING
Route::post('/users/{id}/animals', [AnimalController::class, 'create']);

Route::get('/users/{id}/animals', [AnimalController::class, 'getAll']);

Route::post('users/{id}/animals', [AnimalController::class, 'create']);

Route::put('/animals/{id}', [AnimalController::class, 'update']);

Route::delete('/animals/{id}', [AnimalController::class, 'delete']);

#Species

Route::post('/species', [SpeciesController::class, 'create']);

Route::get('users/{id}/species', [SpeciesController::class, 'getAll']);

Route::put('/species/{id}', [SpeciesController::class, 'update']);

Route::delete('/species/{id}', [SpeciesController::class, 'delete']);
