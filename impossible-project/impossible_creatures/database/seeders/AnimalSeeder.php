<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AnimalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('animals')->insert([
            'species' => "canin",
            'name' => "chien",
            'users_id' => 1,
        ]);
        DB::table('animals')->insert([
            'species' => "félin",
            'name' => "chat",
            'users_id' => 2,
        ]);
        DB::table('animals')->insert([
            'species' => "mammifères",
            'name' => "girafe",
            'users_id' => 3,
        ]);
        DB::table('animals')->insert([
            'species' => "reptile",
            'name' => "tortue",
            'users_id' => 4,
        ]);
        DB::table('animals')->insert([
            'species' => "reptile",
            'name' => "crocodile",
            'users_id' => 5,
        ]);
        DB::table('animals')->insert([
            'species' => "columbidés",
            'name' => "pigeon",
            'users_id' => 6,
        ]);
        DB::table('animals')->insert([
            'species' => "requinance",
            'name' => "requin",
            'users_id' => 7,
        ]);
        DB::table('animals')->insert([
            'species' => "dauphinois",
            'name' => "dauphin",
            'users_id' => 8,
        ]);
    }
}
