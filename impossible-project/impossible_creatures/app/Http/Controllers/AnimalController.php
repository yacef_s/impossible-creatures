<?php

namespace App\Http\Controllers;

use App\Models\Species;
use App\Models\User;
use App\Models\Animal;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Request;


class AnimalController extends Controller
{
    function getAll()
    {
        return (Animal::all());
    }


    function getByID($id)
    {
        return Animal::findOrFail($id);
    }


    public function create(Request $request)
    {
        $validator = Validator::make( $request->all(), [
            'name' => 'required',
            'species' => 'required',

        ]);
        if ($validator->fails()) {
            return response()->json(['status_code' => 400, 'message' => 'Animal not create']);
        }

        $animal = new Animal();
        $animal->name = $request->name;
        $animal->species = $request->species;
        $animal->save();

        return response()->json([
            'status_code' => 200,
            'message' => 'Animal has been created'
        ]);
    }



    function update(Request $request, $id)
    {
        $animal = Animal::findOrFail($id);

        if ($animal) {
            $animal = Animal::updateDTOtoObject($request, $animal);
            $animal->save();
            return response()->json([
                'message' => "New add ."
                ], 200);

        } else {
            return response()->json([
                'message' => "Animal doesn't exist."
            ], 400);
        }
    }

    function delete(Request $request, $id)
    {
        $animal = Animal::findorfail($id);
        if ($animal) {
            $animal->delete();
            return response()->json([
                'message' => "Animal has been deleted ."
            ], 200);
        } else {
            return response()->json([
                'message' => "Animal doesn't exist ."
            ], 400);
        }
    }
}
