<?php

namespace App\Http\Controllers;

use App\Models\Species;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class SpeciesController extends Controller
{
    function getAll()
    {
        return (Species::all());
    }
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'parent' => 'required',
            'user' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status_code' => 400, 'message' => 'Species not create']);
        }

        $species = new Species();
        $species->name = $request->name;
        $species->parent = $request->parent;
        $species->user = $request->user;
        $species->save();

        return response()->json([
            'status_code' => 200,
            'message' => 'species has been created'
        ]);

    }
    function update(Request $request, $id)
    {
        $species = Species::findOrFail($id);

        if ($species) {
            $species = Species::updateDTOtoObject($request, $species);

            $species->save();
            return response()->json([
                'message' => "New add ."
            ], 200);

        } else {
            return response()->json([
                'message' => "Species doesn't exist."
            ], 400);
        }
    }
    function delete(Request $request, $id)
    {
        $species = Species::findorfail($id);
        if ($species) {
            $species->delete();
            return response()->json([
                'message' => "Species has been deleted ."
            ], 200);
        } else {
            return response()->json([
                'message' => "Species doesn't exist ."
            ], 400);
        }
    }

}

