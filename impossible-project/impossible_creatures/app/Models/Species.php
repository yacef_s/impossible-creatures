<?php

namespace App\Models;

use Illuminate\Support\Facades\Request;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Species extends Model

{
    use SoftDeletes;

    static function updateDTOtoObject($request, $species) {
        if ($request->name)
            $species->name = $request->name;
        if ($request->parent)
            $species->parent = $request->parent;
        if ($request->user)
            $species->user = $request->user;

        return ($species);
    }

}
