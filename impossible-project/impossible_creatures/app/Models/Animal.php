<?php

namespace App\Models;

use Illuminate\Support\Facades\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Animal extends Model
{
    use SoftDeletes;

    static function updateDTOtoObject($request, $animal) {
        if ($request->name)
            $animal->name = $request->name;
        if ($request->species)
            $animal->species = $request->species;

        return ($animal);
    }

}
